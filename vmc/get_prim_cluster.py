import requests                         # need this for Get/Post/Delete
import json

strProdURL = "https://vmc.vmware.com"
Refresh_Token = 
ORG_ID = 

def getAccessToken(myKey):
    params = {'refresh_token': myKey}
    headers = {'Content-Type': 'application/json'}
    response = requests.post('https://console.cloud.vmware.com/csp/gateway/am/api/auth/api-tokens/authorize', params=params, headers=headers)
    jsonResponse = response.json()
    access_token = jsonResponse['access_token']
    return access_token

def primary(sddc, tenantid, sessiontoken):
    myHeader = {'csp-auth-token': sessiontoken}
    myURL = strProdURL + "/vmc/api/orgs/" + tenantid + "/sddcs/" + sddc + "/primarycluster"
    response = requests.get(myURL, headers=myHeader)
    print("result =" + str(response.status_code))
    jsonResponse = response.json()
    print(response.content)
    return(primary)

def handler(context, inputs):
    sddc = inputs["sddc"]
    sessiontoken = getAccessToken(Refresh_Token)
    primary(sddc, ORG_ID, sessiontoken)
